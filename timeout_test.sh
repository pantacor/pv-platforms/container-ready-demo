#!/bin/sh

delay=20
echo "sleep time $delay seconds"
echo "sending signal"
sleep $delay
curl -X POST --header "Content-Type: application/json" --data "{\"type\":\"ready\",\"payload\":\"\"}" --unix-socket /pantavisor/pv-ctrl http://localhost/signal

i=0
while [ 1 ]; do
	echo "timeout test message $i"
	i=$((i + 1))
	sleep 10
done
